import torch.optim as optim
import torch.nn.functional as F
import torch.nn as nn
import torch
import torchvision
import matplotlib.pyplot as plt
import random

cuda = torch.device('cuda:0')
download = False
train_batch_size = 1000
test_batch_size = 60

train_data = torchvision.datasets.MNIST('files/', train=True, download=download,
                                        transform=torchvision.transforms.Compose(
                                            [torchvision.transforms.ToTensor(),
                                             torchvision.transforms.Normalize((0.1307,), (0.3081,))]))
# train_data = train_data.cuda()

test_data = torchvision.datasets.MNIST('files/', train=False, download=download,
                                       transform=torchvision.transforms.Compose(
                                           [torchvision.transforms.ToTensor(),
                                            torchvision.transforms.Normalize((0.1307,), (0.3081,))]))
# test_data = test_data.cuda()

# display training data

type(train_data)

train_loader = torch.utils.data.DataLoader(
    train_data, batch_size=train_batch_size, shuffle=False)
test_loader = torch.utils.data.DataLoader(
    test_data, batch_size=test_batch_size, shuffle=False)


class cnn_mnist(nn.Module):
    def __init__(self):
        super(cnn_mnist, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        # self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2(x), 2))
        x = x.view(-1, 320)
        x = torch.sigmoid(self.fc1(x))
        x = F.relu(self.fc2(x))
        return x

    def feature_extract(self, x):
        # x = x.cuda()
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2(x), 2))
        return x

model = cnn_mnist()
model = model.cuda()
def train_featrue_extract(epoch, model, log_interval):
    model.train()
    correct = torch.tensor([0], device=cuda)
    for batch_idx, (data, target) in enumerate(train_loader):
        data = data.cuda()
        target = target.cuda()
        optimizer.zero_grad()
        output = model(data)
        loss = F.cross_entropy(output, target)
        loss.backward()
        optimizer.step()
        pred = output.data.max(1, keepdim=True)[1]
        correct += pred.eq(target.data.view_as(pred)).sum()
    if epoch % log_interval == 0:
        torch.save(model.state_dict(), 'results/model.pth')
        torch.save(optimizer.state_dict(), 'results/optimizer.pth')
        print(
            "Train epoch: {}, loss: {}, accuracy: {}.".format(epoch, loss.item(), correct / len(train_loader.dataset)))



learning_rate = 0.01
momentum = 0.9
optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum)
# optimizer = optim.Adam(model.parameters(), lr=learning_rate)
train_losses = []
train_counter = []
log_interval = 1
epoch = 0

if __name__ == "__main__":
    for i in range(20):
        train_featrue_extract(i, model, 1)
