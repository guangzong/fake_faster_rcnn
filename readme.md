# Fake RPN for object detection


## requirement

following is the package I am using

![](https://raw.githubusercontent.com/chen-gz/picBed/master/20210426093353.png)

## Running the code processs

* run the utils.py file. Before running the code, read the comment in the main function inside utilis.py

* run the main file. Main file will train the network and give the test accuracy base on
generated bounding box. Also it will give some picture to visualize the result.

* enjoy with this small project. If you intresting in object detection. I hope this project will be help
when learning  RPN.


## About this project

This project is for PITT ECE 2372 Pattern Recognition course project. And I have a written proposal and
report for this project.

## Documents

This project is not fully documented. Email the author for more information if you need.
