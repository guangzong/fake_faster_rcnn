from torch import optim

from cnn_for_mnist import cnn_mnist
import torch.nn as nn
from utils import *
from rpn_utils import RPN

import matplotlib.patches as patches


def train_rpn(model, cnn, optimizer, epoch, training_data, bbox_proposals, targets, target_indexes):
    model.train()
    correct = torch.tensor([0])
    losses = 0
    for i in range(training_data.shape[0]):
        data = training_data[i:i + 1]
        bbox_proposal_shrinked, target = get_roi_and_target(targets[i], target_indexes[i], bbox_proposals)
        optimizer.zero_grad()
        bbox_proposal_shrinked = bbox_proposal_shrinked.type(torch.FloatTensor)
        output = model(data, cnn, bbox_proposal_shrinked)
        loss = nn.functional.binary_cross_entropy(output, target)
        # loss = F.binary_cross_entropy
        loss.backward()
        losses += loss.item()
        optimizer.step()
        pred = (output > 0.5).float() * 1
        # pred = pred.cuda()
        correct += pred.eq(target.data.view_as(pred)).sum()
        # print("accuracy: ", correct / ((i + 1) * 8))
    return losses

def save_model(model, optimizer):
    torch.save(model.state_dict(), 'results/rpn_model.pth')
    torch.save(optimizer.state_dict(), 'results/rpn_optimizer.pth')


def evel_rpn(model, cnn, test_data, bbox_proposals, targets):
    model.eval()
    correct = torch.tensor([0])
    for i in range(test_data.shape[0]):
        data = test_data[i:i + 1]
        target = targets[i]
        output = model(data, cnn, bbox_proposals)
        pred = (output > 0.5).float() * 1
        correct += pred.eq(target.data.view_as(pred)).sum()
        ## visualize the result
        if i % 500 == 0:
            fig, ax = plt.subplots()
            ax.imshow(data[0, 0, :, :])
            for j in range(output.shape[0]):
                if pred[j] == 1:
                    rect = patches.Rectangle((bbox_proposals[j][1] * 4, bbox_proposals[j][2] * 4), 28, 28, linewidth=1,
                                             edgecolor='r', facecolor='none')
                    ax.add_patch(rect)
            plt.show()

    print("accuracy: ", correct / ((i + 1) * bbox_proposals.shape[0]))


if __name__ == "__main__":
    cnn = cnn_mnist()
    cnn.load_state_dict(torch.load('results/model.pth'))
    cnn.eval()
    bbox_anchors, bbox_proposals = generate_bbox_proposal()
    train_data, train_bbox, test_data, test_bbox = load_train_test_data()
    train_target, train_indexes, test_target, test_indexes = load_targets_and_indexes()
    bbox_proposal_shrinked, target_new = get_roi_and_target(train_target[0], train_indexes[0], bbox_proposals)
    rpn = RPN()
    learning_rate = 0.01
    momentum = 0.9
    optimizer = optim.SGD(rpn.parameters(), lr=learning_rate, momentum=momentum)
    # # training RPN
    # # train rpn 2 epoch
    losses = []
    for i in range(2):
        loss = train_rpn(rpn, cnn, optimizer, 0, train_data, bbox_proposals, train_target, train_indexes)
        losses.append(loss)
    save_model(rpn, optimizer)
    # visualize losses
    plt.plot(losses)
    plt.show()

    #  load parameter instead of train RPN
    rpn.load_state_dict(torch.load('results/rpn_model.pth'))

    evel_rpn(rpn, cnn, test_data, bbox_proposals, test_target)
