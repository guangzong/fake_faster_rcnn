import torch
import random
import torchvision
import pickle

import matplotlib.pyplot as plt


def get_random_points_on_canvas(random_number: int = 2, canvas_size: int = 64, picture_size: int = 28) -> (list, list):
    x_list = []
    y_list = []
    while len(x_list) < random_number:
        x_list = []
        y_list = []
        attempt_times = 50
        attempt_cnt = 0
        while attempt_cnt < attempt_times and len(x_list) < random_number:
            attempt_cnt += 1
            feasible = True
            a = random.randint(0, canvas_size - picture_size)
            b = random.randint(0, canvas_size - picture_size)
            for i in range(len(x_list)):
                if a >= x_list[i] - picture_size and a <= x_list[i] + picture_size and \
                        b >= y_list[i] - picture_size and b <= y_list[i] + picture_size:
                    feasible = False
                else:
                    pass
            if feasible:
                x_list.append(a)
                y_list.append(b)

    return x_list, y_list


def save_train_test_data_to_file(is_train_data=True, download=False):
    train_batch_size = 4
    test_batch_size = 3
    canvas_size = 4 * 16
    figures_in_canvas_num = train_batch_size

    origin_data = torchvision.datasets.MNIST('files/', train=is_train_data, download=download,
                                             transform=torchvision.transforms.Compose(
                                                 [torchvision.transforms.ToTensor(),
                                                  torchvision.transforms.Normalize((0.1307,), (0.3081,))]))

    # test_data = torchvision.datasets.MNIST('files/', train=False, download=download,
    #                                        transform=torchvision.transforms.Compose(
    #                                            [torchvision.transforms.ToTensor(),
    #                                             torchvision.transforms.Normalize((0.1307,), (0.3081,))]))

    data_loader = torch.utils.data.DataLoader(origin_data, batch_size=train_batch_size, shuffle=True)
    # test_loader = torch.utils.data.DataLoader(test_data, batch_size=test_batch_size, shuffle=True)

    example_number = int(len(origin_data) / train_batch_size)

    generated_figure = torch.zeros((example_number, 1, canvas_size, canvas_size), dtype=torch.float32)
    generated_bbox = torch.zeros((example_number, figures_in_canvas_num, 4))

    sample_counter = 0

    picture_size = 28
    for batch_idx, (example_data, example_targets) in enumerate(data_loader):
        print(batch_idx)
        # if batch_idx >= 10:  # for debug
        #     break           # for debug
        x_list, y_list = get_random_points_on_canvas(example_data.shape[0], canvas_size=canvas_size)
        picture_on_canvas_number = min(len(x_list), example_data.shape[0])
        if len(x_list) != example_data.shape[0]:
            print("canvas too small")

        w_list = [28] * picture_on_canvas_number
        h_list = [28] * picture_on_canvas_number

        # train_figure is the input we will use
        for i in range(picture_on_canvas_number):
            generated_figure[sample_counter, 0, x_list[i]:x_list[i] + picture_size,
            y_list[i]:y_list[i] + picture_size] = example_data[i][0]

        # y_training (target)
        train_bounding_box = torch.tensor(
            [x_list, y_list, w_list, h_list], dtype=torch.float32)
        train_bounding_box = torch.transpose(train_bounding_box, 0, 1)
        generated_bbox[sample_counter] = train_bounding_box

        # plt.imshow(train_figure[sample_counter][0],
        #            cmap='gray', interpolation='none')
        # plt.show()
        sample_counter += 1

    print("save training and test data")
    if is_train_data:
        torch.save(generated_figure, "train_data.pt")
        torch.save(generated_bbox, "train_bbox.pt")
    else:
        torch.save(generated_figure, "test_data.pt")
        torch.save(generated_bbox, "test_bbox.pt")
    return generated_figure, generated_bbox


def load_train_test_data():
    train_data = torch.load("train_data.pt")
    train_bbox = torch.load("train_bbox.pt")
    test_data = torch.load("test_data.pt")
    test_bbox = torch.load("test_bbox.pt")
    return train_data, train_bbox, test_data, test_bbox


def generate_bbox_proposal():
    # 64 anchor from 7 -- 64-7 box-size 7*7
    bbox_anchors = torch.zeros((225, 2), dtype=torch.int32)
    bbox_proposals = torch.zeros((225, 5), dtype=torch.float)
    bbox_cnt = 0
    for i in range(3, 64 - 4, 4):
        for j in range(3, 64 - 4, 4):
            bbox_anchors[bbox_cnt, :] = torch.tensor([i, j])
            bbox_proposals[bbox_cnt, :] = torch.tensor([0, i - 3, j - 3, i + 4, j + 4])
            bbox_cnt += 1
    return bbox_anchors, bbox_proposals


def get_roi_and_target(target, target_index, bbox_proposals):
    bbox_proposal_num = len(target_index) * 2
    target_new = torch.zeros((bbox_proposal_num, 1))

    bbox_proposal_shrinked = torch.zeros((bbox_proposal_num, 5), dtype=float)
    index_perm = torch.randperm(bbox_proposal_num)
    for i in range(len(target_index)):
        bbox_proposal_shrinked[index_perm[i], :] = bbox_proposals[target_index[i]]
        target_new[index_perm[i], 0] = 1
    for i in range(len(target_index), bbox_proposal_num):
        tmp = target_index[0]
        while target[tmp, 0] == 1:
            tmp = random.randint(0, target.shape[0] - 1)
        bbox_proposal_shrinked[index_perm[i], :] = bbox_proposals[tmp]

    return bbox_proposal_shrinked, target_new


def get_target(ground_bbox, generate_bbox):
    # train_bounding_boxes = train_bounding_boxes.cuda()
    # generated_bounding_boxes = generated_bounding_boxes.cuda()
    target = torch.zeros((225, 1))
    target_index = []
    # target = target.cuda()
    for i in range(generate_bbox.shape[0]):
        have = 0
        for j in range(ground_bbox.shape[0]):
            if ground_bbox[j, 0] + 0.25 * 28 >= generate_bbox[i, 1] * 4 >= ground_bbox[
                j, 0] - 0.25 * 28 \
                    and ground_bbox[j, 1] + 0.25 * 28 >= generate_bbox[i, 2] * 4 >= \
                    ground_bbox[j, 1] - 0.25 * 28:
                have = 1
        target[i, 0] = have
        if have == 1:
            target_index.append(i)
    # target_index = torch.tensor(target_index, dtype=int)
    return target, target_index


def get_targets(train_bbox, bbox_proposals):
    targets = torch.zeros(train_bbox.shape[0], 225, 1)
    targets_index = []
    print(train_bbox.shape[0])
    for i in range(train_bbox.shape[0]):
        print(i)
        test_target, test_target_index = get_target(train_bbox[i], bbox_proposals)
        targets[i, :, :] = test_target
        targets_index.append((test_target_index))
    return targets, targets_index


def load_targets_and_indexes():
    with open("train_target_index.pt", "rb") as fp:
        train_indexes = pickle.load(fp)
    with open("test_target_index.pt", "rb") as fp:
        test_indexes = pickle.load(fp)
    train_targets = torch.load("train_targets.pt")
    test_targets = torch.load("test_targets.pt")
    return train_targets, train_indexes, test_targets, test_indexes


def save_train_target(train_targets, train_indexes):
    with open("train_target_index.pt", "wb") as fp:
        pickle.dump(train_indexes, fp)
    torch.save(train_targets, "train_targets.pt")


def save_test_target(test_targets, test_indexes):
    with open("test_target_index.pt", "wb") as fp:
        pickle.dump(test_indexes, fp)
    torch.save(test_targets, "test_targets.pt")


if __name__ == "__main__":
    ### first part
    # first time run the code, generate dataset and save them
    # if you do not have the MNIST data,  change download to True
    # if you download from the github, just skip first part
    # train_data, train_bbox = save_train_test_data_to_file(is_train_data=True, download=False)
    # test_data, test_bbox = save_train_test_data_to_file(is_train_data=False, download=False)
    # bbox_anchors, bbox_proposals = generate_bbox_proposal()
    # train_targets, train_indexes = get_targets(train_bbox, bbox_proposals)
    # test_targets, test_indexes = get_targets(test_bbox, bbox_proposals)
    # save_train_target(train_targets, train_indexes)
    # save_test_target(test_targets, test_indexes)

    ##### second part
    # Just read the dataset from the file
    train_data, train_bbox, test_data, test_bbox = load_train_test_data()
    bbox_anchors, bbox_proposals = generate_bbox_proposal()
    train_targets, train_indexes, test_targets, test_indexes = load_targets_and_indexes()

    ### check the dimension of data to make sure it is correct processed

    print(train_data.shape)
    print(train_bbox.shape)
    print(train_targets.shape)
    print(len(train_indexes))

    print(test_data.shape)
    print(test_bbox.shape)
    print(test_targets.shape)
    print(len(test_indexes))

