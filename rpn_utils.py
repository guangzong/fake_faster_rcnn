import torch
import torch.nn as nn
import torch.functional as F
import torchvision
import cnn_for_mnist

class RPN(nn.Module):
    def __init__(self):
        super(RPN, self).__init__()
        # self.conv1 = nn.Conv2d(1, 10, kernel_size=5, padding=2)
        # self.conv2 = nn.Conv2d(10, 10, kernel_size=5, padding=2)
        self.fc1 = nn.Linear(20 * 5 * 5, 50)
        self.fc2 = nn.Linear(50, 1)

    def forward(self, x, cnn_mnist, rois):
        # x = x.cuda()
        x = cnn_mnist.feature_extract(x)
        # x = F.relu(F.max_pool2d(self.conv1(x), 2))
        # x = F.relu(F.max_pool2d(self.conv2(x), 2))

        x = torchvision.ops.roi_pool(x, rois, [5, 5])
        x = x.view(-1, 500)
        x = torch.sigmoid(self.fc1(x))
        x = torch.sigmoid(self.fc2(x))
        return x

